APLICAȚIE WEB PENTRU MANAGEMENTUL UNUI CABINET MEDICAL

Proiectul poate fi găsit pe Git-ul din link-ul de mai jos:
https://gitlab.upt.ro/andreea.coste/proiect-licenta

Pentru a putea porni proiectul aveți nevoie de:

1. XAMPP
2. MySQL




Pentru a pori proiectul:

Se copiază fișierele in %locație XAMPP%/htdocs 

Se importa baza de date din fișierul office.sql cu ajutorul phpMyAdmin.

După importul bazei de date aplicația se poate accesa la adresa http://localhost
