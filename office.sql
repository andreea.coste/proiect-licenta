-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2024 at 11:54 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `office`
--
CREATE DATABASE IF NOT EXISTS `office` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `office`;

-- --------------------------------------------------------

--
-- Table structure for table `articol`
--

CREATE TABLE `articol` (
  `id` int(11) NOT NULL,
  `titlu` varchar(255) NOT NULL,
  `continut` text NOT NULL,
  `imagine` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `publicare` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `articol`
--

INSERT INTO `articol` (`id`, `titlu`, `continut`, `imagine`, `categorie`, `publicare`) VALUES
(1, 'Speranta si recunostina', '<div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a </span><u style=\"font-size: 1rem;\"><strike>type </strike></u><span style=\"font-size: 1rem;\">specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passag</span><i style=\"font-size: 1rem;\">es, and more recently with desktop<u> p</u>ublishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the i</i><span style=\"font-size: 1rem;\">ndustry\'s stand</span><b style=\"font-size: 1rem;\">ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not <u>only</u> five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of </b><span style=\"font-size: 1rem;\">Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></div>', 'holding.jpg', 'Categorie1', '2023-06-08'),
(2, 'Atacul de panica', '<div style=\"text-align: justify;\"><ol><li><span style=\"font-size: 1rem;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Â </span></li><li><span style=\"font-size: 1rem;\">Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry.Â </span></li><li><span style=\"font-size: 1rem;\">Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Â </span></li><li><span style=\"font-size: 1rem;\">It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Â </span></li><li><span style=\"font-size: 1rem;\">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></li></ol></div>', 'blur.jpg', 'categorie2', '2023-06-07'),
(3, 'Cum apare depresia?', '<div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\r\n</span><u style=\"font-size: 1rem;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </u><span style=\"font-size: 1rem;\">Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></div>', 'one.jpg', 'categoriee', '2023-06-04'),
(4, 'Depresia la persoanele varstnice', '<div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</span></div>', 'man.jpg', 'categorie4', '2023-06-05'),
(5, 'Ruminarea', '<div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</span></div>', 'show.jpg', 'categorie5', '2023-05-10'),
(6, 'Tulburarea obsesiv-compulsiva', '<div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">Obsesiile sunt ganduri, impulsuri sau imagini mentale repetitive, incontrolabile, care provoaca anxietate. Exemple: teama de microbi sau contaminare, teama de a contacta o boala, ganduri agresive fata de sine/ altii, teama de a nu face rau cuiva apropiat, teama de a nu uita efectuarea unui comportament precum inchiderea usii sau a gazului etc.\r\n\r\n \r\n\r\nCompulsiile reprezinta comportamente repetitive pe care persoana simte ca trebuie sa le realizeze ca rapuns la gandurile obsesive. Exemple: verificare, curatare excesiva, repetare unei actiuni, aranjarea obiectelor intr-o anumita ordine, etc. Persoana care sufera de TOC petrece in general mai mult de o ora pe zi preocupata fiind de ganduri si comportamente precum cele de mai sus. Aceste ritualuri/ actiuni nu-i fac placere, dar dupa realizarea lor simte o scurta eliberare de anxietatea provocata de ganduri. Majoritatea celor afectati isi dau seama ca temerile pe care le au nu sunt pe deplin reale.</span></div>', 'mental.jpg', 'categorie', '2023-05-10'),
(7, 'Trauma', '<div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">Ce este trauma?\r\n \r\nIn urma expunerii la un eveniment traumatizant aproximativ 20% dintre oameni dezvolta pe termen lung simptome precum rememorari ale traumei initiale, cosmaruri, evitarea stimulilor asociati cu trauma, hipervigilenta (Riedesser & Fischer, 2007). In  acest caz, evenimentele traumatice afecteaza deopotriva corpul si mintea, persoana afectata reactionand ca si cum evenimentul traumatizant inca se petrece. Trauma continua sa deranjeze realitatea vizuala, auditiva si/ sau orice alta realitate somatica a vietii victimelor sale (Rothschild, 2015).  \r\n\r\nPot aparea alternari de perioade de activare excesiva si de epuizare, iar la aparitia neasteptata a unor elemente care amintesc de trauma se instaleaza panica. De asemenea, este posibila instalarea imobilitatii sau a disocierii, mecanisme de supravietuire la care psihicul apeleaza in conditiile nedezvoltarii altora mai adaptative.Â </span></div><div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">Astfel, desi in sine inadecvate, aceste mecanisme au in fond un important rol de protectie. In timpul evenimentului traumatic poate sa apara clivajul constiintei (disocierea) ca reactie instinctiva de a proteja sinele in fata suferintei, fiind posibil ca unele persoane sa isi aminteasca traumele doar ca emotii puternice sau senzatii corporale.\r\n\r\nRepetam ceea ce nu reparam\r\n\r\nPredispozitia pentru dificultatea de a gestiona stresul se afla, probabil, in evenimente din copilaria mica precum neglijare, abuz fizic si sexual, relatie de atasament inadecvata cu ingrijitorul primar. Din fericire, deficitele asociate tulburarilor de atasament pot fi in mare masura compensate cu ajutorul unor persoane semnificative precum cel mai bun prieten, un profesor deosebit sau alta persoana apropiata care ofera alinare.\r\n\r\n \r\n\r\nIn procesul de vindecare a traumei cu ajutorul psihoterapiei corpul reprezinta o resursa importanta. Pacientul este insotit sa o acceseze prin intermediul constientizarii senzatiilor corporale pe fondul unei stari de calm.\r\n\r\nIn viata cotidiana persoana traumatizata poate sa isi stabileasca oaze, adica sa se implice in activitati care necesita concentrare, reducand hiperactivarea si intensitatea dialogului interior si facand-o sa uite de trauma.Â </span></div><div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">Exemple: gradinarit, citit, tricotat, reparatii etc. Utile sunt si ancorele, adica resurse observabile precum o persoana, un obiect, un animal sau o activitate care confera o stare de alinare corporala si emotionala.</span></div>', 'water.jpg', 'Mental health', '2023-06-09'),
(8, 'Stresul dupa trauma', '<div style=\"text-align: justify;\"><span style=\"font-size: 1rem;\">Trauma este inteleasa ca suferinta care fragmenteaza mecanismele de aparare ale psihicului si corpului, generand uneori perturbari de durata. \r\n\r\nDe ce apare suferinta posttraumatica?\r\n\r\nFactorii etiologici in suferinta posttraumatica sunt preponderent exogeni. Printre evenimentele potential psihotraumatice se numara: experienta proprie a abuzului fizic/ sexual sau asistarea la acesta, violenta in familii sau comunitati, moartea accidentala sau violenta a unei persoane iubite, ranire sau boala care pun in pericol propria viata, razboiul, terorismul, dezastrele naturale, opresiunea sistematica de tipul rasismului. Astfel de stresori extremi genereaza stari de excitatie ale sistemului nervos central care in functie de vulnerabilitatea individuala pot duce la simptome ireversibile si leziuni psihice pe termen lung. Nu toate persoanele expuse la evenimente traumatice dezvolta simptome persistente ale traumei. Este posibila elaborarea in mod natural a experientei traumatice, evitand astfel ancorarea in patologie. Evenimentul care se dovedeste traumatic este perceput de subiect ca fiind brusc, necontrolabil si negativ. 10% dintre persoanele traumatizate dezvolta simptome durabile.\r\nRaspunsul la stres si trauma este reglat de sistemul limbic, sistemul nervos vegetativ (SNV) si de axa hipotalamica-pituitara-adrenala. Stresorul activeaza SNV generand semne somatice de tipul paloare, diminuarea digestiei, dilatarea pupilelor, accelerarea ritmului cardiac respirator, cresterea presiunii sangvine si transpiratie, durere, oboseala, tensiune musculara, hipervigilenta, alterarea functionarii creierului, simptome afective (frica, depresie, furie, schimbari frecvente de dispozitie), comportamentale (evitarea lucrurilor care amintesc de situatia traumatica) si cognitive (credinte irationale, rigide despre sine, altii, lume). \r\nCorespondenta in plan psihopatologic a mecanismului fiziologic de reactie la stimulul stresant este descrisa de asa-numitul spectru al starilor de stres posttraumatic. In viata civila acestea sunt: reactia acuta de stres (simptome prezente mai putin de patru zile), tulburarea acuta de stres (simptome clinic semnificative mai putin de o luna) si stresul posttraumatic acut (simptome mentinute intre una si trei luni) / cronic (simptome prezente mai mult de trei luni). Dintre acestea, tulburarea de stres posttraumatic (Posttraumatic Stress Disorder - PTSD) este in prezent cea mai cercetata. Criteriile diagnostice ale tulburarii de stres posttraumatic au fost incluse in DSM in 1981 ca urmare a dezbaterilor starnite de manifestarile veteranilor razboiului din Vietnam. Acestea includ rememorari ale traumei initiale, cosmaruri, evitarea stimulilor asociati cu trauma, hipervigilenta. O serie de studii care au evidentiat in cazul adolescentilor legatura dintre expunerea la evenimente traumatice si depresie, respectiv anxietate si faptul ca strategiile active de coping precum cautarea suportului diminueaza aceste simptome. Un alt studiu demonstreaza ca stresul posttraumatic mediaza relatia dintre trauma si simptomele de depresie si anxietate la tinerii aflati in detentie. \r\n\r\nNeuropsihologia traumei\r\nUn element cheie in dezvoltarea si mentinerea simptomatologiei PTSD il reprezinta dereglarea memoriei. Persoana evita in mod constient amintirile evenimentului traumatic, insa ele sunt activate in mod automat intr-o forma intruziva. In explicarea acestui fapt, Exista o distinctie intre memoria ï¿½receï¿½ si cea ï¿½fierbinteï¿½. Prima are ca substrat regiunea hipocampica din sistemul limbic si se refera la categorii precum timpul, spatiul si cauzalitatea, limba, fapte, descrieri, naratiuni (memoria explicita, declarativa), pe cand cea de-a doua este atribuita amigdalei cerebrale si stocheaza in functie de relevanta emotionala informatie preponderent senzoriala (memoria implicita, nondeclarativa). Stimulii amenintatori activeaza centrii de dirijare din hipotalamus ai sistemului nervos autonom si duc la eliberarea de hormoni de stres. Regiunea hipocampica si cortexul cingular sunt inhibate in starile extreme de excitatie, nemaijucand astfel rolul de filtru. In plus, exista o activare mai intensa a emisferei drepte (orientata spre caracteristicile globale ale comunicarii si perceptia non-verbala) comparativ cu cea stanga (gandire analitica, rezolvare de probleme). Se retin astfel in experienta peritraumatica impresii senzoriale decontextualizate, dar detaliate, care ï¿½ingheataï¿½ in memorie si sunt reactualizate in mod intruziv, repetitiv, chiar decenii, daca sunt stimulate situativ de stimuli senzoriali sau simptome somatice care amintesc de trauma. Din cauza suprimarii activitatii de mediere a hipocampului, sistemul memoriei explicite nu poate stoca o naratiune a evenimentului produs. Pe termen lung se poate vorbi de disfunctionalitatea sistemului limbic care, prin nivelul prea scazut al cortizolului, face ca supraexcitatia sa apara si in absenta amenintarii.</span></div>', 'hands.jpg', 'Recuperare', '2023-06-02'),
(9, 'Comunicarea dincolo de cuvinte', '<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">Ne aflam intr-o permanenta baie de semnale pe care le emitem si le receptam permanent, ceea ce ne permite sa ne conectam cu viata din jurul nostru. Fiecare organism viu detine un camp energetic complex obtinut prin compunerea mai multor campuri energetice care apartin elementelor din structura sa, din aproape in aproape, pornind de la energia de camp a moleculelor. Astfel, plantele, animalele si oamenii sunt inconjurati ca o anvelopa de un biocamp unic, emitand radiatii electromagnetice si fiind totodata influentati in evolutia campului lor energetic, favorabil sau nociv, de mediul in care se afla. Biocampul se propaga sub forma de unde.</div></blockquote><div style=\"text-align: justify;\">Experimental s-a demonstrat ca exista un fenomen de biocomunicatie prin care spre exemplu celulele din acelasi lot, desi plasate separat, comunica intre ele, informandu-se cu privire la starea lor de sanatate. Fenomenul de biocomunicatie a fost folosit pentru a explica de ce plantele reactioneaza la gandurile si intentiile noastre. Ele sesizeaza prezenta campului energetic emis de om si reactioneaza in concordanta cuÂ  semnificatia si valentele sale afective. O serie de experimente au relevat ca in fata unui pericol plantele reactioneaza violent, pe cand la contactul cu un om care le trateaza cu gentilete raspunsul este bland, fapt redat de miscarile unui osciloscop conectat.</div><div style=\"text-align: justify;\">In acelasi registru, se pare ca si la oameni exista o comunicare in afara limbajului, doar prin forta gandului. Vorbim de vreo doua milioane de ani, dupa ce mult mai mult de atat am tacut si totusi am relationat eficient. Aceasta ar putea insemna ca si oamenii au dispus de un prelimbaj telepatic ce s-a atrofiat odata cu aparitia limbajului articulat. Probabil ca fiecare dintre noi mai pastreaza o forma latenta a acestui tip de comunicare.</div><div style=\"text-align: justify;\">Cum functioneaza ea? Informatia elaborata in creierul emitatorului se presupune ca se poate transmite prin vehiculul campului energetic la receptor. In experimentele efectuate s-a observat ca receptorul are nevoie de o limitare a stimulilor veniti din afara organismului si de o ingustare a campului constiintei. De asemenea s-au inregistrat modificari obiective in cursul fenomenului telepatic precum scaderea rezistentei electrice la nivelul pielii, adica o crestere a conductibilitatii electrice cutanate, inclusiv in zonele cu rol de â€antenaâ€ din organism (ex. crestetul capului), ceea ce se intampla de fapt in mod normal in plan fiziologic atunci cand traimÂ  emotii. Acest fapt poate insa fi interpretat si ca o facilitare a posibilitatilor de transmitere si receptie. Relaxarea psihica se pare ca este o conditie indeosebi pentru receptor in telepatia provocata experimental, in timp ce in cea spontana s-a remarcat ca receptorul este intr-o stare de mare tensiune afectiva. Totodata, s-a concluzionat ca putem primi informatii care raman cantonate doar in subconstient, putandu-ne totusi influenta.</div><div style=\"text-align: justify;\">In cursul experientelor de biocomunicare, la creierul receptor ajunge exact informatia emisa, asa cum a fost formulata. Gandul poate fi considerat o forta materiala, adica un camp energetic. De aceea, autorii preocupati de aceata tema afirma ca un oarecare gand, chiar nemarturisit, poate in anumite conditii sa provoace o actiune atunci cand este receptat de alt creier.</div><div style=\"text-align: justify;\">Comunicarea biologica bazata pe un transfer de energie interuman se pare ca nu este influentata de distanta. Sunt binecunoscute numeroase situatii in care persoane intre care exista o legatura afectiva (ex. membrii ai unei familii) simt suferinta celui apropiat, in acelasi fel, nefiind in proximitate si neputand comunica pe canalele uzuale. Exista studii in cursul carora fenomenul de transmitere nu a fost perturbat de distante de mii de km, fiind mai pregnant la nivel de imagine si impuls.</div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\"><i>Aceste realitati au totusi o actiune subtila si sunt prea putin intelese. Ele nu anuleaza liberul arbitru, capacitatea noastra de autodeterminare, ci dimpotriva, pot constitui un imbold de a alege cu grija ce fel de ganduri si emotii nutrim si transmitem in jur. Prin aceasta ne influentam propriile relatii cu lumea in care traim.</i></div>', 'eye.jpg', 'Categorie', '2023-06-10');

-- --------------------------------------------------------

--
-- Table structure for table `conversatie`
--

CREATE TABLE `conversatie` (
  `id` int(11) NOT NULL,
  `id_utilizator_medic` int(11) NOT NULL,
  `id_utilizator_pacient` int(11) NOT NULL,
  `creare` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `conversatie`
--

INSERT INTO `conversatie` (`id`, `id_utilizator_medic`, `id_utilizator_pacient`, `creare`) VALUES
(20, 10, 32, '2023-06-29 18:43:04'),
(21, 10, 93, '2024-08-21 14:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `id_utilizator_medic` int(11) NOT NULL,
  `id_utilizator_pacient` int(11) NOT NULL,
  `fisier` varchar(255) NOT NULL,
  `numeFisier` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `id_utilizator_medic`, `id_utilizator_pacient`, `fisier`, `numeFisier`) VALUES
(15, 10, 32, 'bb6a212-hands.jpg', 'hands.jpg'),
(16, 10, 32, '15021ac-undraw_profile_3.svg', 'undraw_profile_3.svg'),
(18, 10, 32, '32a9750-sample.pdf', 'sample.pdf'),
(23, 10, 32, '4c0428f-undraw_profile_2.svg', 'undraw_profile_2.svg'),
(25, 10, 32, '777afbd-pdf-sample.pdf', 'pdf-sample.pdf'),
(30, 33, 80, '92b1577-sample.pdf', 'sample.pdf'),
(31, 33, 80, '3a3acf8-pdf-sample.pdf', 'pdf-sample.pdf'),
(32, 33, 80, '07a68ff-logo.png', 'logo.png'),
(33, 33, 80, 'a337894-undraw_profile_2.svg', 'undraw_profile_2.svg'),
(34, 33, 80, 'c8f6c02-sample.pdf', 'sample.pdf'),
(35, 33, 80, 'b8ac659-file-sample_100kB.docx', 'file-sample_100kB.docx'),
(36, 33, 80, 'e38c366-file-sample_100kB.doc', 'file-sample_100kB.doc'),
(37, 33, 80, '54204a9-undraw_profile_1.svg', 'undraw_profile_1.svg');

-- --------------------------------------------------------

--
-- Table structure for table `intrebare`
--

CREATE TABLE `intrebare` (
  `id` int(11) NOT NULL,
  `id_medic` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  `continut` text NOT NULL,
  `creare` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `intrebare`
--

INSERT INTO `intrebare` (`id`, `id_medic`, `id_test`, `continut`, `creare`) VALUES
(24, 4, 22, '<ul><li>Prima parte</li><li>A doua parte. Nulla facilisi.Â </li><li>Praesent vel justo ex. Vestibulum ut aliquet quam. Donec non erat et quam placerat mollis sit amet ac enim. Aliquam eros lacus, eleifend sit amet condimentum a, facilisis a diam.Â </li></ul>', '2023-06-29 16:23:31'),
(25, 4, 22, 'Phasellus at magna sit amet tortor euismod vestibulum. In interdum, tortor eu accumsan porta, lacus nunc lobortis est, et varius ante magna eget elit. Vestibulum sagittis elementum sem, in efficitur ipsum posuere ac. Nam quam enim, dictum sodales mauris vel, rhoncus commodo massa. Etiam risus sapien, ullamcorper a tellus iaculis, viverra pharetra?', '2023-06-29 16:23:52'),
(26, 4, 22, '<i>Vivamus sagittis</i>, ante ac vestibulum tincidunt, mauris nisi ullamcorper urna, eget dapibus lorem dolor vitae erat. Aenean vehicula faucibus eros, nec cursus sem hendrerit nec. Fusce fermentum, leo cursus tempus iaculis, sem elit rhoncus purus, <u>eu tempor augue magna ultrices mauris</u>?', '2023-06-29 16:24:59'),
(27, 4, 23, '<div>Phasellus at magna sit amet tortor euismod vestibulum. In interdum, tortor eu accumsan porta, lacus nunc lobortis est, et varius ante magna eget elit. Vestibulum sagittis elementum sem, in efficitur ipsum posuere ac. Nam quam enim, dictum sodales mauris vel, rhoncus commodo massa?</div>', '2023-06-29 16:25:49'),
(28, 4, 23, '<div>Etiam risus sapien, ullamcorper a tellus iaculis, viverra pharetra ipsum. Curabitur quis odio at ante accumsan convallis. Sed tincidunt neque turpis, eget sagittis nulla sodales eu?</div><div><br></div>', '2023-06-29 16:26:03'),
(29, 4, 23, '<div>Maecenas convallis blandit aliquam. <b>Interdum et malesuada fames ac ante ipsum primis in faucibus</b>. Nullam iaculis dolor in tortor semper lobortis. Maecenas id diam in sapien mattis sollicitudin ut sed lectus. Duis sed tempor odio. Phasellus sed tristique diam, vel vehicula lacus?</div><div><br></div>', '2023-06-29 16:26:31'),
(30, 4, 23, '<div>Nullam iaculis dolor in tortor semper lobortis. Maecenas id diam in sapien mattis sollicitudin ut sed lectus. <i><u>Duis sed tempor odio</u></i>. Phasellus sed tristique diam, vel vehicula lacus.</div>', '2023-06-29 16:26:57');

-- --------------------------------------------------------

--
-- Table structure for table `medic`
--

CREATE TABLE `medic` (
  `id` int(11) NOT NULL,
  `id_utilizator` int(11) DEFAULT NULL,
  `specialitate` varchar(255) NOT NULL,
  `data_nasterii` date DEFAULT NULL,
  `telefon` varchar(255) NOT NULL,
  `adresa` varchar(255) NOT NULL,
  `imagine` varchar(255) NOT NULL,
  `creare` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `medic`
--

INSERT INTO `medic` (`id`, `id_utilizator`, `specialitate`, `data_nasterii`, `telefon`, `adresa`, `imagine`, `creare`) VALUES
(4, 10, 'Gastro', '1960-03-14', '0747049332', 'str. Victoriei 14 Bucuresti', 'fbdb961-medic2.jpg', '2024-08-21 14:40:22'),
(15, 33, 'Gastroenterolog', '1980-04-09', '07685395', 'str Victoriei 33 Oradea', '955f167-medic1.jpg', '2024-08-21 14:38:02'),
(17, 35, 'Gastroenterolog', '1943-04-02', '0238787152', 'str. A. I. Cuza 7 Iasi', '3de7df5-medic3.jpg', '2024-08-21 14:41:03');

-- --------------------------------------------------------

--
-- Table structure for table `mesaj`
--

CREATE TABLE `mesaj` (
  `id` int(11) NOT NULL,
  `id_conversatie` int(11) NOT NULL,
  `id_utilizator` int(11) NOT NULL,
  `continut` varchar(255) NOT NULL,
  `fisier` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`fisier`)),
  `creare` datetime NOT NULL,
  `vazut` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `mesaj`
--

INSERT INTO `mesaj` (`id`, `id_conversatie`, `id_utilizator`, `continut`, `fisier`, `creare`, `vazut`) VALUES
(12, 20, 10, 'Hello', 'null', '2023-06-29 18:47:26', 1),
(13, 20, 32, 'Hi!', 'null', '2023-06-29 18:47:46', 1),
(14, 20, 10, 'test', 'null', '2023-06-30 07:54:26', 1),
(38, 20, 10, 'Uite un pdf', '[\"ee53884-sample.pdf\"]', '2023-07-06 16:52:23', 1),
(39, 20, 32, 'Va trimit niste documente word:', '[\"9ac1275-file-sample_100kB.doc\", \"934be22-file-sample_100kB.docx\"]', '2023-07-06 16:53:35', 1),
(40, 20, 32, 'si niste poze', '[\"cddba02-undraw_profile_3.svg\", \"cc2963b-undraw_profile_2.svg\"]', '2023-07-06 16:53:54', 1),
(45, 20, 10, 'Multumesc', '[]', '2023-07-06 17:11:19', 1),
(46, 21, 93, 'buna seara! va deranjez cu o mica problema!', '[]', '2024-08-21 15:05:18', 1),
(47, 21, 10, 'buna seara! cu ce va pot ajuta?', '[]', '2024-08-21 15:06:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pacient`
--

CREATE TABLE `pacient` (
  `id` int(11) NOT NULL,
  `id_utilizator` int(11) DEFAULT NULL,
  `id_medic` int(11) NOT NULL,
  `data_nasterii` date NOT NULL,
  `gen` varchar(255) NOT NULL,
  `telefon` varchar(255) NOT NULL,
  `adresa` varchar(255) NOT NULL,
  `creare` datetime NOT NULL,
  `bunic_p` text DEFAULT NULL,
  `bunica_p` text DEFAULT NULL,
  `bunic_m` text DEFAULT NULL,
  `bunica_m` text DEFAULT NULL,
  `tata` text DEFAULT NULL,
  `mama` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `pacient`
--

INSERT INTO `pacient` (`id`, `id_utilizator`, `id_medic`, `data_nasterii`, `gen`, `telefon`, `adresa`, `creare`, `bunic_p`, `bunica_p`, `bunic_m`, `bunica_m`, `tata`, `mama`) VALUES
(10, 32, 4, '1977-05-15', 'masculin', '25747248', 'zfjbybyky', '2023-07-21 09:51:03', '', 'Boala', 'Conditie', '', 'Sanatos', 'Alzheimer'),
(18, 93, 4, '1997-10-12', 'masculin', '09786543', 'tydtucfvkhbj', '2023-07-25 20:39:14', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 80, 15, '1994-10-21', 'masculin', '07543253647', 'str. AAA nr. 76 Oras', '2023-07-19 14:21:46', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 81, 15, '1993-12-14', 'masculin', '0746525768', 'str CC nr 17 Oras', '2023-07-19 18:18:56', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pacienttest`
--

CREATE TABLE `pacienttest` (
  `id` int(11) NOT NULL,
  `id_pacient` int(11) NOT NULL,
  `id_test` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `pacienttest`
--

INSERT INTO `pacienttest` (`id`, `id_pacient`, `id_test`) VALUES
(18, 10, 22),
(19, 10, 23),
(25, 18, 22);

-- --------------------------------------------------------

--
-- Table structure for table `raspuns`
--

CREATE TABLE `raspuns` (
  `id` int(11) NOT NULL,
  `id_pacient` int(11) NOT NULL,
  `id_intrebare` int(11) NOT NULL,
  `continut` text NOT NULL,
  `creare` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `raspuns`
--

INSERT INTO `raspuns` (`id`, `id_pacient`, `id_intrebare`, `continut`, `creare`) VALUES
(1, 10, 24, '<p>Raspuns Vestibulum sagittis elementum sem, in efficitur ipsum posuere ac. Nam quam enim, dictum sodales mauris vel, rhoncus commodo massa. Etiam risus sapien, ullamcorper a tellus iaculis, viverra pharetra.</p>', '2023-06-29 16:53:42'),
(2, 10, 26, '<p>Raspuns mhsj ysyek vkejrvr jfjyrst ry vktydytsjgvjh</p>', '2023-06-29 16:56:02'),
(8, 10, 27, '<p>Duis sed tempor odio. Phasellus sed tristique diam.</p>', '2023-07-19 12:26:46'),
(9, 10, 28, '<p>Nam quam enim, dictum sodales mauris vel.</p>', '2023-07-19 12:26:54'),
(10, 10, 29, '<p>Da.</p>', '2023-07-19 12:26:57'),
(11, 10, 30, '<p class=\"ql-align-justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultrices at tellus at dapibus. Mauris quis risus suscipit, posuere erat eu, volutpat libero. Cras rhoncus scelerisque ex, at placerat eros pulvinar eu. Suspendisse potenti. Phasellus consectetur, erat eu mattis molestie, urna elit tempor risus, non vulputate lectus tellus vestibulum turpis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque pharetra dapibus lorem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc id est sit amet eros volutpat auctor. Vivamus volutpat ullamcorper tortor vitae aliquam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce at odio leo. Nam cursus metus dignissim consectetur dapibus. Vivamus ac velit posuere, dapibus nunc eget, lobortis sapien.</p>', '2023-07-20 18:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `id_medic` int(11) NOT NULL,
  `denumire` varchar(255) NOT NULL,
  `descriere` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `id_medic`, `denumire`, `descriere`) VALUES
(22, 4, 'Test 1 MA', '<div><b style=\"font-size: 1rem;\">Lorem ipsum</b><span style=\"font-size: 1rem;\"> dolor sit amet, consectetur adipiscing elit. Praesent vel justo ex. Vestibulum ut aliquet quam. Donec non erat et quam placerat mollis sit amet ac enim. Nulla facilisi. Aliquam eros lacus, eleifend sit amet condimentum a, facilisis a diam.Â </span></div>'),
(23, 4, 'Test 2 MA', 'Aenean vehicula faucibus eros, nec cursus sem hendrerit nec. Fusce fermentum, leo cursus tempus iaculis, sem elit rhoncus purus, eu tempor augue magna ultrices mauris');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `titlu` varchar(255) NOT NULL,
  `continut` text NOT NULL,
  `rating` enum('1','2','3','4','5') NOT NULL,
  `nume` varchar(255) NOT NULL,
  `despre` varchar(255) NOT NULL,
  `imagine` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `titlu`, `continut`, `rating`, `nume`, `despre`, `imagine`) VALUES
(1, 'Extraordinar!', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '5', 'Alexandra Xiculescu', 'Client', 'f40ae50-profile.jpg'),
(2, 'M-am simtit eliberat!', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\"', '4', 'Omer Tetik', 'CEO Banca Transilvania', '5e7a21f-aa.jpg'),
(3, 'Plin de compasiune', '\"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '2', 'Vladimir Pop', 'Client', '61b8fff-pic.jpg'),
(6, 'Incredible personnel!', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', '3', 'Michael A. Hack', 'JCD at Metropolitan Tribunal Chicago', '9a68ffd-people-who-loves.png');

-- --------------------------------------------------------

--
-- Table structure for table `utilizator`
--

CREATE TABLE `utilizator` (
  `id` int(11) NOT NULL,
  `nume` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `parola` varchar(255) NOT NULL,
  `roluri` enum('ROL_ADMIN','ROL_MEDIC','ROL_PACIENT') NOT NULL DEFAULT 'ROL_PACIENT',
  `activ` tinyint(1) NOT NULL DEFAULT 0,
  `hash_activare` varchar(255) DEFAULT NULL,
  `hash_resetare` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `utilizator`
--

INSERT INTO `utilizator` (`id`, `nume`, `email`, `parola`, `roluri`, `activ`, `hash_activare`, `hash_resetare`) VALUES
(1, NULL, 'admin@mail.com', '$2y$10$dKi2XzcSIdZg4P3WVUkYh./u/HEOagXlEQZ.1TCJ.CPsTpc5Z54ke', 'ROL_ADMIN', 1, NULL, NULL),
(10, 'Mihaela Anitei', 'mihai1@gmail.com', '$2y$10$As/oWmqdAR44y484L.XAkePZ7Ieq2NXTDb642rKnBYn6.xJLp7yji', 'ROL_MEDIC', 1, NULL, NULL),
(32, 'Nume Pacient13', 'pac@m.m', '$2y$10$.FoFmDMI5EGRyBZleA9V3eeYkWV/xAJrP66jDFHBjxPw7anNUzkw6', 'ROL_PACIENT', 1, NULL, NULL),
(33, 'Ana Pop', 'medic2@m.m', '$2y$10$U8/.CbuK47H18HYA7FjUuuVRopkZD4eheZjn6fnEl1hzaT2u/EJza', 'ROL_MEDIC', 1, NULL, NULL),
(35, 'Eduard Gruber', 'eduard@e.e', '$2y$10$MQ/sg6vBAdu1MjnUS8Ap/OtxGw5Ydvc60dd3I.QyFTKzfgge7h36y', 'ROL_MEDIC', 1, NULL, NULL),
(80, 'Pacient AA 1', 'pacaslanaa@mailinator.com', '$2y$10$6mB6VNveteFZo1vvV7.o3.KmyV7v6Stum5l3/FI0TgHL1KqoAsKPi', 'ROL_PACIENT', 1, NULL, NULL),
(81, 'Pacient AA 2', 'mailpac@mailinator.com', '$2y$10$6mB6VNveteFZo1vvV7.o3.KmyV7v6Stum5l3/FI0TgHL1KqoAsKPi', 'ROL_PACIENT', 1, NULL, NULL),
(93, 'Pacient test MA 1', 'maillpacient1@mailinator.com', '$2y$10$dOomynf4d.lher8OnjTDXeFHQa5OHjvISGskVKM8FFvcsYQwLo2m2', 'ROL_PACIENT', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articol`
--
ALTER TABLE `articol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversatie`
--
ALTER TABLE `conversatie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_emitator` (`id_utilizator_medic`),
  ADD KEY `id_receptor` (`id_utilizator_pacient`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_utilizator_medic` (`id_utilizator_medic`),
  ADD KEY `id_utilizator_pacient` (`id_utilizator_pacient`);

--
-- Indexes for table `intrebare`
--
ALTER TABLE `intrebare`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Intrebare_ibfk_2` (`id_medic`),
  ADD KEY `id_test` (`id_test`);

--
-- Indexes for table `medic`
--
ALTER TABLE `medic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Medic_ibfk_1` (`id_utilizator`);

--
-- Indexes for table `mesaj`
--
ALTER TABLE `mesaj`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_conversatie` (`id_conversatie`),
  ADD KEY `id_utilizator` (`id_utilizator`);

--
-- Indexes for table `pacient`
--
ALTER TABLE `pacient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_medic` (`id_medic`),
  ADD KEY `id_utilizator` (`id_utilizator`);

--
-- Indexes for table `pacienttest`
--
ALTER TABLE `pacienttest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pacient` (`id_pacient`),
  ADD KEY `id_test` (`id_test`);

--
-- Indexes for table `raspuns`
--
ALTER TABLE `raspuns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_intrebare` (`id_intrebare`),
  ADD KEY `id_pacient` (`id_pacient`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Test_ibfk_2` (`id_medic`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utilizator`
--
ALTER TABLE `utilizator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articol`
--
ALTER TABLE `articol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `conversatie`
--
ALTER TABLE `conversatie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `intrebare`
--
ALTER TABLE `intrebare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `medic`
--
ALTER TABLE `medic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `mesaj`
--
ALTER TABLE `mesaj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `pacient`
--
ALTER TABLE `pacient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pacienttest`
--
ALTER TABLE `pacienttest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `raspuns`
--
ALTER TABLE `raspuns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `utilizator`
--
ALTER TABLE `utilizator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `conversatie`
--
ALTER TABLE `conversatie`
  ADD CONSTRAINT `Conversatie_ibfk_1` FOREIGN KEY (`id_utilizator_medic`) REFERENCES `utilizator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Conversatie_ibfk_2` FOREIGN KEY (`id_utilizator_pacient`) REFERENCES `utilizator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `Document_ibfk_1` FOREIGN KEY (`id_utilizator_medic`) REFERENCES `utilizator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Document_ibfk_2` FOREIGN KEY (`id_utilizator_pacient`) REFERENCES `utilizator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `intrebare`
--
ALTER TABLE `intrebare`
  ADD CONSTRAINT `Intrebare_ibfk_2` FOREIGN KEY (`id_medic`) REFERENCES `medic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Intrebare_ibfk_3` FOREIGN KEY (`id_test`) REFERENCES `test` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `medic`
--
ALTER TABLE `medic`
  ADD CONSTRAINT `Medic_ibfk_1` FOREIGN KEY (`id_utilizator`) REFERENCES `utilizator` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `mesaj`
--
ALTER TABLE `mesaj`
  ADD CONSTRAINT `Mesaj_ibfk_1` FOREIGN KEY (`id_conversatie`) REFERENCES `conversatie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Mesaj_ibfk_2` FOREIGN KEY (`id_utilizator`) REFERENCES `utilizator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pacient`
--
ALTER TABLE `pacient`
  ADD CONSTRAINT `Pacient_ibfk_1` FOREIGN KEY (`id_medic`) REFERENCES `medic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Pacient_ibfk_2` FOREIGN KEY (`id_utilizator`) REFERENCES `utilizator` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `pacienttest`
--
ALTER TABLE `pacienttest`
  ADD CONSTRAINT `PacientTest_ibfk_1` FOREIGN KEY (`id_pacient`) REFERENCES `pacient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `PacientTest_ibfk_2` FOREIGN KEY (`id_test`) REFERENCES `test` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `raspuns`
--
ALTER TABLE `raspuns`
  ADD CONSTRAINT `Raspuns_ibfk_1` FOREIGN KEY (`id_intrebare`) REFERENCES `intrebare` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Raspuns_ibfk_2` FOREIGN KEY (`id_pacient`) REFERENCES `pacient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `Test_ibfk_2` FOREIGN KEY (`id_medic`) REFERENCES `medic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
